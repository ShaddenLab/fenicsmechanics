import fenicsmechanics as fm
from fenicsmechanics import fmio
from fenicsmechanics import fmlogging

fm_handlers = fmlogging.create_handlers(stream_level=fm.LogLevel.TRACE,
                                        file_level=fm.LogLevel.DEBUG,
                                        logname="TEST-fm_progress.out")

import logging
logging.basicConfig(handlers=fm_handlers)

fmlogging.init_logs(stream_level=fm.LogLevel.TRACE,
                    file_level=fm.LogLevel.DEBUG,
                    logname="logs/fm_progress.out")
logger = fmlogging.get_user_logger(logname=None, stream_level=fm.LogLevel.TRACE)

logger.info("Getting mesh file names")
mesh_file, boundaries = fm.get_mesh_file_names("lshape", ret_facets=True,
                                               refinements="fine")
config = {
    'material':
    {
        'type': 'elastic',
        'const_eqn': 'neo_hookean',
        'incompressible': True,
        'kappa': 10e9,
        'mu': 1.5e6
    },
    'mesh': {
        'mesh_file': mesh_file,
        'boundaries': boundaries
    },
    'formulation': {
        'element': 'p1-p1',
        'domain': 'lagrangian',
        'inverse': True,
        'bcs': {
            'dirichlet': {
                'displacement': [[0., 0.]],
                'regions': [1]
            },
            'neumann': {
                'values': [[0., -1e5]],
                'regions': [2],
                'types': ['cauchy']
            }
        }
    }
}

fname_unloaded = "results/unloaded_config.pvd"

# First solve the inverse elastostatics problem.
logger.trace("Creating problem and solver objects.")
problem = fm.SolidMechanicsProblem(config)
solver = fm.SolidMechanicsSolver(problem, fname_disp=fname_unloaded)

logger.trace("About to solve the problem.")
solver.full_solve()

# Move the mesh using dolfin's ALE functionality
from dolfin import ALE, Mesh
ALE.move(problem.mesh, problem.displacement)
mesh_copy = Mesh(problem.mesh)

# Only need to change relevant entries in config
config['mesh']['mesh_file'] = mesh_copy
config['formulation']['inverse'] = False

fname_loaded = "results/loaded_config.pvd"

# Solve a 'forward' problem.
problem = fm.SolidMechanicsProblem(config)
solver = fm.SolidMechanicsSolver(problem, fname_disp=fname_loaded)
solver.full_solve()

# Patch to circumvent incompatibility between dolfin and Paraview.
fmio.rewrite_vtk_files(fname_unloaded)
fmio.rewrite_vtk_files(fname_loaded)

# Cleaning up file names since this is not a time series.
fmio.clean_up_pvd_files(fname_unloaded)
fmio.clean_up_pvd_files(fname_loaded)
