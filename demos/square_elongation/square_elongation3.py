import numpy as np

import dolfin as dlf
import fenicsmechanics as fm
from fenicsmechanics import fmio
from fenicsmechanics.__CONSTANTS__ import unit_domain_ids, unit_domain_labels

material = {
    'density': 1e3,
    'type': 'elastic',
    'const_eqn': 'neo_hookean',
    'incompressible': True,
    'kappa': 10e9, # Pa
    'mu': 1.5e6 # Pa
}

mesh_file, boundaries = fm.get_mesh_file_names("unit_domain", ret_facets=True,
                                               refinements=[20, 20])
mesh = {
    'mesh_file': mesh_file,
    'boundaries': boundaries
}

p_max = 1e6
pbar_x_after1 = "max(-{a}*pow(t - 2.0, 5), 0.0)"
pbar_x = "t <= 1.0 ? {a}*t : " + pbar_x_after1
pbar_x = pbar_x.format(a=p_max)
pbar = [pbar_x, "0.0"]

zero_vec = [0.0, 0.0]
zero_tensor = fm.dlf.Constant([[0.0, 0.0], [0.0, 0.0]])

formulation = {
    'element': 'p2-p1',
    'domain': 'lagrangian',
    'time': {
        'dt': 0.01,
        'interval': [0.0, 4.0]
    },
    'bcs': {
        'weak': {
            'values': [zero_vec, zero_vec, zero_tensor],
            'regions': [unit_domain_ids.LEFT, "everywhere", "everywhere"],
            'types': ["displacement", "velocity", "gradv"],
            'coeffs': [1e9, 2e3, 1e0],
            'components': ["all", "all", "all"]
        },
        'neumann': {
            'values': [pbar],
            'regions': [unit_domain_ids.RIGHT],
            'types': ['piola']
        }
    }
}

config = {
    'material': material,
    'mesh': mesh,
    'formulation': formulation
}

fname_config = "results/time_dependent/config.log"
fname_disp = "results/time_dependent/displacement-time_dependent-weak_bcs.pvd"

problem = fm.SolidMechanicsProblem(config)
problem.write_config(fname_config)

solver = fm.SolidMechanicsSolver(problem, fname_disp=fname_disp)

# Catch exception if it occurs, rewrite VTK files, and then
# re-raise exception.
try:
    solver.full_solve()
except RuntimeError as e:
    # Patch to circumvent incompatibility between dolfin and Paraview.
    fmio.rewrite_vtk_files(fname_disp)
    raise e

# Patch to circumvent incompatibility between dolfin and Paraview.
fmio.rewrite_vtk_files(fname_disp)
