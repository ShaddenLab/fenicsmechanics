import fenicsmechanics as fm
from fenicsmechanics import fmio

material = {
    'type': 'elastic',
    'const_eqn': 'neo_hookean',
    'incompressible': True,
    'kappa': 10e9, # Pa
    'mu': 1.5e6 # Pa
}

mesh_file, boundaries = fm.get_mesh_file_names("unit_domain", ret_facets=True,
                                               refinements=[20, 20])
mesh = {
    'mesh_file': mesh_file,
    'boundaries': boundaries
}

formulation = {
    'element': 'p2-p1',
    'domain': 'lagrangian',
    'bcs': {
        'weak': {
            'values': [[0.0, 0.0]],
            'regions': [1],
            'types': ["displacement"],
            'coeffs': [1e9],
            'components': ["all"]
        },
        'neumann': {
            'values': [[1e6, 0.]],
            'regions': [2],
            'types': ['piola']
        }
    }
}

config = {
    'material': material,
    'mesh': mesh,
    'formulation': formulation
}

fname_config = "results/weak_bcs/config.log"
fname_disp = "results/weak_bcs/displacement-weak_bcs.pvd"

problem = fm.SolidMechanicsProblem(config)
problem.write_config(fname_config)

solver = fm.SolidMechanicsSolver(problem, fname_disp=fname_disp)
solver.full_solve()

# Patch to circumvent incompatibility between dolfin and Paraview.
fmio.rewrite_vtk_files(fname_disp)
fmio.clean_up_pvd_files(fname_disp)
