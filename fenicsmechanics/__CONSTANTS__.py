dict_implemented = {
    'materials': {
        'elastic': ('lin_elastic', 'neo_hookean', 'demiray', 'guccione', 'fung', 'holzapfel_ogden'),
        'viscous': ('newtonian', 'stokes')
    }
}


unit_domain_labels = {
    0: "interior",
    1: "x = 0",
    2: "x = 1",
    3: "y = 0",
    4: "y = 1",
    5: "z = 0",
    6: "z = 1"
}

from argparse import Namespace as _Namespace
unit_domain_ids = _Namespace(ALL_ELSE=0,
                             LEFT=1,
                             RIGHT=2,
                             BOTTOM=3,
                             TOP=4,
                             BACK=5,
                             FRONT=6)

lv_ids = _Namespace(ALL_ELSE=0,
                    BASE=10,
                    ENDO=20,
                    EPI=30,
                    APEX=40)

import os as _os
_dirname = _os.path.dirname(_os.path.realpath(__file__))
base_mesh_dir = _os.path.abspath(_os.path.join(_dirname, "../meshfiles"))

del _os, _dirname, _Namespace
