from .fmlogging import get_logger as _get_logger

_logger = _get_logger("fm.fmconversions", logname=None)


def dolfin_to_vtk(func):
    """
    Convert a dolfin.Function object to a VTK unstructured grid.


    Parameters
    ----------

    func : dolfin.Function
        The FEM approximation to a function.


    Returns
    -------

    ugrid : vtk.vtkUnstructuredGrid
        The unstructured grid object containing the mesh, and field data.

    """
    V = func.function_space()
    dofmap = V.dofmap()
    degree = V.ufl_element().degree()
    mesh = V.mesh()
    geo_dim = mesh.geometry().dim()

    cell_type, permutation = get_vtk_cell_type(V)

    n_cells = mesh.num_cells()
    np_connectivity = get_vtk_connectivity_matrix(V)
    nodes_per_element = _get_num_nodes_per_cell(dofmap)
    vtk_cells = get_vtk_cell_array(np_connectivity, permutation=permutation)

    val_shape = V.ufl_element().value_shape()
    nrank = len(val_shape)
    if nrank == 0:
        step = 1
    elif nrank == 1:
        step = val_shape[0]
    else:
        raise NotImplementedError

    np_coordinates = V.tabulate_dof_coordinates()[::step]
    vtk_points = get_vtk_points(np_coordinates, geo_dim=geo_dim)

    vtk_vec_vals = get_vtk_vector_array(func)

    if V.ufl_element().shortstr()[:3] == "DG0":
        data_type = "cells"
    else:
        data_type = "points"

    ugrid = create_vtk_unstructured_grid(vtk_points, vtk_cells, cell_type,
                                         vtk_vec_vals, data_type)

    return ugrid


def vtk_to_dolfin(ugrid, array_idx=None, data_type="points", geo_dim=None):
    """
    Convert a VTK unstructured grid (or poly data) into a dolfin.Function
    object.

    Parameters
    ----------

    ugrid : vtk.vtkUnstructuredGrid, vtk.vtkPolyData
        The unstructured grid object containing the mesh, and field data.
    array_idx : int, str (default None)
        The index or name of the array to use for building the
        FEM function.
    data_type : str (default "points")
        Specify if the data to be used for the conversion is point data
        ("points") or cell data ("cells").
    geo_dim : int (default None)
        Override the geometric dimension extracted by 'extract_geo_dim'.
        Useful when the user wants to include all z coordinates for a
        planar mesh.

    Returns
    -------

    func : dolfin.Function
        The FEM function generated from data stored in the vtk unstructured
        grid or poly data.

    """
    ugrid = convert_polydata_to_ugrid(ugrid)
    cellname, degree = get_cellname_and_degree(ugrid)
    if degree <= 1:
        func = vtk_to_dolfin_on_vertices_cells(ugrid,
                                               array_idx=array_idx,
                                               data_type=data_type,
                                               geo_dim=geo_dim)
    else:
        msg = "Converting a VTK unstructured grid with degree {0} " \
            + "to a dolfin function is not supported."
        raise NotImplementedError(msg.format(degree))

    return func


def dolfin_to_vtk_on_vertices_cells(func):
    """
    Convert a dolfin.Function object to a VTK unstructured grid by only
    using the function values at the vertices or cells. Note that this is
    the way dolfin writes functions to VTK files.

    Parameters
    ----------

    func : dolfin.Function
        The FEM approximation to a function.

    Returns
    -------

    ugrid : vtk.vtkUnstructuredGrid
        The unstructured grid object containing the mesh, and field data.

    """
    from vtk.util.numpy_support import numpy_to_vtk

    V = func.function_space()
    mesh = V.mesh()
    geo_dim = mesh.geometry().dim()
    degree = V.ufl_element().degree()

    # Overwrite the degree of the cell since we are only converting on
    # mesh vertices and cells.
    if degree > 1:
        degree_overwrite = 1
    else:
        degree_overwrite = None
    temp_ret = get_vtk_cell_type(V, degree_overwrite=degree_overwrite)
    cell_type, permutation = temp_ret

    ncells = mesh.num_cells()
    nverts = mesh.num_vertices()

    connectivity = mesh.cells()
    coords = mesh.coordinates()

    vtk_points = get_vtk_points(coords, geo_dim=geo_dim)
    vtk_cells = get_vtk_cell_array(connectivity, permutation=permutation)

    shape = func.ufl_shape
    nrank = len(shape)
    if nrank == 0:
        ncomponents = 1
    elif nrank == 1:
        ncomponents = shape[0]
    else:
        msg = "Field variable with rank greater than 1 is not supported."
        raise NotImplementedError(msg)

    if degree == 0:
        func_vec_vals = func.vector().get_local().reshape([-1, ncomponents])
    else:
        func_vec_vals = func.compute_vertex_values()
        func_vec_vals = func_vec_vals.reshape([ncomponents, -1]).T

    if nrank == 1:
        func_vec_vals = _pad_array(func_vec_vals, geo_dim)

    vtk_vec_vals = numpy_to_vtk(func_vec_vals)
    vtk_vec_vals.SetName(func.name())

    ugrid = create_vtk_unstructured_grid(vtk_points, vtk_cells, cell_type,
                                         vtk_vec_vals, data_type="points")

    return ugrid


def vtk_to_dolfin_on_vertices_cells(ugrid, array_idx=None,
                                    data_type="points", geo_dim=None):
    """
    Convert a VTK unstructured grid (or poly data) into a dolfin.Function
    object using only the values at vertices or cells.

    Parameters
    ----------

    ugrid : vtk.vtkUnstructuredGrid, vtk.vtkPolyData
        The unstructured grid object containing the mesh, and field data.
    array_idx : int, str (default None)
        The index or name of the array to use for building the
        FEM function.
    data_type : str (default "points")
        Specify if the data to be used for the conversion is point data
        ("points") or cell data ("cells").
    geo_dim : int (default None)
        Override the geometric dimension extracted by 'extract_geo_dim'.
        Useful when the user wants to include all z coordinates for a
        planar mesh.

    Returns
    -------

    func : dolfin.Function
        The FEM function generated from data stored in the vtk unstructured
        grid or poly data.

    """
    ugrid = convert_polydata_to_ugrid(ugrid)

    cellname, degree = get_cellname_and_degree(ugrid)
    coords, connectivity = extract_mesh_attributes(ugrid)
    mesh = create_dolfin_mesh(coords, connectivity, cellname,
                              degree=degree, geo_dim=geo_dim)

    vtk_vec_array = extract_field_data_array(ugrid, array_idx=array_idx,
                                             data_type=data_type)
    func = create_dolfin_function(mesh, vtk_vec_array)

    return func


def multiple_dolfin_to_single_vtk(*dlf_functions):
    import vtk
    from vtk.util.numpy_support import numpy_to_vtk
    for i, dlf_func in enumerate(dlf_functions):
        if i == 0:
            mesh0 = dlf_func.function_space().mesh()
            geo_dim = mesh0.geometry().dim()
            funcname0 = dlf_func.name()
            vtk_multi = dolfin_to_vtk_on_vertices_cells(dlf_func)

            n_pts = vtk_multi.GetNumberOfPoints()
            n_cells = vtk_multi.GetNumberOfCells()
        else:

            if dlf_func.function_space().mesh() != mesh0:
                funcname = dlf_func.name()
                msg = "The mesh for the first function ('{0}') does not match " \
                    + "the mesh for the {1}-th function ('{2}')"
                raise ValueError(msg.format(funcname0, i+1, funcname))

            # TODO: This code is essentially a repeat of
            #     'dolfin_to_vtk_on_vertices_cells'
            # Find a better way to write this. Maybe move this portion
            # into a function?
            degree = dlf_func.function_space().ufl_element().degree()
            shape = dlf_func.ufl_shape
            nrank = len(shape)
            if nrank == 0:
                ncomponents = 1
            elif nrank == 1:
                ncomponents = shape[0]
            else:
                msg = "Field variable with rank greater than 1 is not supported."
                raise NotImplementedError(msg)

            if degree == 0:
                func_vec_vals = dlf_func.vector().get_local().reshape([-1, ncomponents])
            else:
                func_vec_vals = dlf_func.compute_vertex_values()
                func_vec_vals = func_vec_vals.reshape([ncomponents, -1]).T

            if nrank == 1:
                func_vec_vals = _pad_array(func_vec_vals, geo_dim)

            vtk_vec_vals = numpy_to_vtk(func_vec_vals)
            vtk_vec_vals.SetName(dlf_func.name())

            n_tuples = vtk_vec_vals.GetNumberOfTuples()
            if n_tuples == n_pts:
                vtk_multi.GetPointData().AddArray(vtk_vec_vals)
            elif n_tuples == n_cells:
                vtk_multi.GetCellData().AddArray(vtk_vec_vals)
            else:
                msg = "DEV: the number of tuples in the new array does not" \
                    + "match the number of vertices or cells. Should not " \
                    + "be in this 'else' block."
                raise ValueError(msg)

    return vtk_multi


def convert_polydata_to_ugrid(pd):
    """
    Convert a vtk.vtkPolyData object to a vtk.vtkUnstructuredGrid
    object. This is done by using the VTK append filter. Note that
    no changes occur if a vtk.vtkUnstructuredGrid object is provided.

    Parameters
    ----------

    pd : vtk.vtkPolyData, vtk.vtkUnstructuredGrid
        The object to convert to a VTK unstructured grid.

    Returns
    -------

    ugrid : vtk.vtkUnstructuredGrid
        The VTK unstructured grid.

    """
    import vtk

    if isinstance(pd, vtk.vtkUnstructuredGrid):
        ugrid = pd
    else:
        append_filter = vtk.vtkAppendFilter()
        append_filter.AddInputData(pd)
        append_filter.Update()

        ugrid = append_filter.GetOutput()

    return ugrid


def get_cellname_and_degree(ugrid):
    """
    Get the dolfin cell name and the polynomial degree for a given
    VTK cell type.

    Parameters
    ----------

    ugrid : vtk.vtkUnstructuredGrid, vtk.vtkPolyData
        The unstructured grid object containing the mesh, and field data.

    Returns
    -------

    cellname : str
        The corresponding name of the cell used by dolfin.
    degree : int
        The polynomial degree used by dolfin.

    """
    import numpy as np
    from vtk.util.numpy_support import vtk_to_numpy
    celltypes = vtk_to_numpy(ugrid.GetCellTypesArray())
    unique_celltypes = np.array(list(set(celltypes)))
    if unique_celltypes.size > 1:
        msg = "Only one unique cell type per unstructured grid is supported."
        raise ValueError(msg)
    else:
        vtk_celltype = unique_celltypes[0]

    cellname, degree = vtk_to_dlf_mappings[vtk_celltype]

    return cellname, degree


def extract_mesh_attributes(ugrid):
    """
    Extract the coordinates for the mesh vertices and the connectivity
    matrix.

    Parameters
    ----------

    ugrid : vtk.vtkUnstructuredGrid, vtk.vtkPolyData
        The unstructured grid object containing the mesh, and field data.

    Returns
    -------

    coords : numpy.ndarray
        The coordinates of the mesh vertices contained in 'ugrid'.
    connectivity : numpy.ndarray
        The connectivity matrix for the mesh contained in 'ugrid'.

    """
    from vtk.util.numpy_support import vtk_to_numpy

    ncells = ugrid.GetNumberOfCells()

    vtk_coords = ugrid.GetPoints().GetData()
    coords = vtk_to_numpy(vtk_coords)

    vtk_connectivity = ugrid.GetCells().GetData()
    tmp = vtk_to_numpy(vtk_connectivity).reshape([ncells, -1])
    connectivity = tmp[:, 1:]

    return coords, connectivity


def extract_field_data_array(ugrid, array_idx=None, data_type="points"):
    """
    Extract point or cell data from the VTK unstructured grid. Note that
    the user must specify which data type to use (cells or points). If no
    array index is provided, the first array will be used.

    Parameters
    ----------

    ugrid : vtk.vtkUnstructuredGrid, vtk.vtkPolyData
        The unstructured grid object containing the mesh, and field data.
    array_idx : int, str (default None)
        The index or name of the array to use for building the
        FEM function.
    data_type : str (default "points")
        Specify if the data to be used for the conversion is point data
        ("points") or cell data ("cells").

    Returns
    -------

    vtk_array : vtk.vtkDataArray
        The VTK array containing the data of interest.

    """
    if data_type == "points":
        data = ugrid.GetPointData()
    elif data_type == "cells":
        data = ugrid.GetCellData()
    else:
        msg = "The data type '{0}' is not supported.".format(data_type)
        raise ValueError(msg)

    if array_idx is None:
        msg = "No array index was specified. Using the first with " \
            + "name '{0}'.".format(data.GetArrayName(0))
        _logger.warning(msg)
        array_idx = 0

    # Note that this will already return 'None' is the array
    # does not exist.
    vtk_array = data.GetArray(array_idx)

    return vtk_array


def extract_top_geo_dims(coords, cellname):
    """
    Extract the topological and geometric dimensions given the set of
    coordinates and the cell name.

    Parameters
    ----------

    coords : numpy.ndarray
        Array storing vertex coordinates.
    cellname : str
        The cell name used by dolfin.

    Returns
    -------

    top_dim : int
        The topological dimension for the cell name given.
    geo_dim : int
        The geometric dimension based on the number of zero columns.

    """
    from ufl import Cell

    if isinstance(cellname, Cell):
        cellname = cellname.cellname()
    top_dim = topological_dims[cellname]

    geo_dim = extract_geo_dim(coords)

    return top_dim, geo_dim


def extract_geo_dim(coords):
    """
    Extract the geometric dimensions given the set of coordinates. A check
    on the last two columns is done to see if the entire column is set to zero.
    If so, the geometric dimension is reduced by the number of zero columns.

    This is used due to the fact the VTK always stores 3 coordinate components,
    even if all vertices lie on a coordinate plane.

    Parameters
    ----------

    coords : numpy.ndarray
        Array storing vertex coordinates.

    Returns
    -------

    geo_dim : int
        The geometric dimension based on the number of zero columns.

    """
    import numpy as np

    geo_dim = 3
    zvals = coords[:, 2]
    yvals = coords[:, 1]
    if np.all(zvals == 0.0):
        geo_dim -= 1
        if np.all(yvals == 0.0):
            geo_dim -= 1

    return geo_dim


def create_dolfin_mesh(coords, connectivity, cellname, degree=1, geo_dim=None):
    """
    Create a dolfin mesh given the coordinates of the vertices, connectivity,
    the cell name, and the mesh degree.

    Note: a mesh degree other than 1 is currently not supported.

    Parameters
    ----------

    coords : numpy.ndarray
        Array storing vertex coordinates.
    connectivity : numpy.ndarray
        The connectivity matrix for the mesh.
    cellname : str
        The cell name to be used by dolfin.
    degree : int (default 1)
        The mesh degree.
    geo_dim : int (default None)
        Override the geometric dimension extracted by 'extract_geo_dim'.
        Useful when the user wants to include all z coordinates for a
        planar mesh.

    Returns
    -------

    mesh : dolfin.Mesh
        The dolfin mesh object generated.

    """
    import dolfin as dlf
    from ufl import Cell

    if degree != 1:
        msg = "Mesh degree of {0} is currently not supported.".format(degree)
        raise NotImplementedError(msg)

    if coords.ndim != 2:
        raise ValueError("Coordinate array must be 2-dimensional.")

    if connectivity.ndim != 2:
        raise ValueError("Connectivity array must be 2-dimensional.")

    if isinstance(cellname, Cell):
        cellname = cellname.cellname()

    if geo_dim is None:
        top_dim, geo_dim = extract_top_geo_dims(coords, cellname)
    else:
        top_dim, _ = extract_top_geo_dims(coords, cellname)

    nverts = coords.shape[0]
    ncells = connectivity.shape[0]

    mesh = dlf.Mesh()
    meditor = dlf.MeshEditor()
    meditor.open(mesh, cellname, top_dim, geo_dim, degree)
    meditor.init_vertices(nverts)
    meditor.init_cells(ncells)

    for i, pt_coords in enumerate(coords[:, :geo_dim]):
        meditor.add_vertex(i, pt_coords)

    for i, cell_connectivity in enumerate(connectivity):
        meditor.add_cell(i, cell_connectivity)

    meditor.close()

    return mesh


def create_dolfin_function(mesh, vtk_vec_array):
    """
    Create a dolfin.Function object given a mesh and a VTK array object
    storing the coefficients of the FEM function.

    Parameters
    ----------

    mesh : dolfin.Mesh
        The dolfin mesh used to define a function space.
    vtk_vec_array : vtk.vtkDataArray
        The VTK array object storing the coefficients of the FEM function.

    Returns
    -------

    func : dolfin.Function
        The FEM function generated with the mesh and VTK array.

    """
    import dolfin as dlf
    from vtk.util.numpy_support import vtk_to_numpy

    ncomponents = vtk_vec_array.GetNumberOfComponents()
    nverts = mesh.num_vertices()
    ncells = mesh.num_cells()

    ntuples = vtk_vec_array.GetNumberOfTuples()
    if ntuples == nverts:
        family = "CG"
    elif ntuples == ncells:
        family = "DG"
    else:
        msg = "The number of tuples in the VTK vector array ({0}) does " \
            + "not match the number of vertices ({1}) or cells ({2}) for " \
            + "the mesh provided."
        raise ValueError(msg.format(ntuples, nverts, ncells))

    geo_dim = mesh.geometry().dim()
    if ncomponents > 1:
        tmp_vec_array = vtk_to_numpy(vtk_vec_array)[:, :geo_dim]
    else:
        tmp_vec_array = vtk_to_numpy(vtk_vec_array)
    vec_array = tmp_vec_array.flatten()

    if ncomponents == 1:
        element = dlf.FiniteElement(family, mesh.ufl_cell(), 1)
    else:
        element = dlf.VectorElement(family, mesh.ufl_cell(), 1)
    V = dlf.FunctionSpace(mesh, element)
    func = dlf.Function(V)

    d2v = dlf.dof_to_vertex_map(V)
    func.vector()[:] = vec_array[d2v]

    name = vtk_vec_array.GetName()
    func.rename(name, name)

    return func


# def vtk_to_dolfin(ugrid):
#     """
#     """

#     ncells = ugrid.GetNumberOfCells()
#     npts = ugrid.GetNumberOfPoints()

#     tmp_output = get_mesh_topology_info(ugrid)
#     cell_type, geo_dim, top_dim, mesh_node_indices = tmp_output

#     np_coords, np_connectivity = extract_mesh_attributes(ugrid)
#     mesh = create_dlf_mesh(np_coords, np_connectivity, mesh_node_indices)
#     family, degree, n_components = get_field_data_params(ugrid)

#     if n_components == geo_dim:
#         element = dlf.VectorElement(family, mesh.ufl_cell(), degree)
#     elif n_components == 1:
#         element = dlf.FiniteElement(family, mesh.ufl_cell(), degree)
#     else:
#         msg = "The number of components ({0}) and geometric dimension ({1}) " \
#             + "is not understood."
#         raise ValuError(msg.format(n_components, geo_dim))

#     V = dlf.FunctionSpace(mesh, element)
#     func = dlf.Function(V)

#     func_vec = extract_function_vector(ugrid)
#     _ = assign_vec_to_func(func_vec, func)

#     return func


def get_vtk_connectivity_matrix(V):
    """
    Return the connectivity matrix for a dolfin.FunctionSpace object as a
    2D numpy array.


    Parameters
    ----------

    V : dolfin.FunctionSpace
        The function space to extract the DOF connectivity matrix from.


    Returns
    -------

    connectivity : numpy.ndarray
        The resulting connectivity matrix for use with VTK data types.

    """
    import numpy as np

    # Climb down the sub function spaces until we are at the end of the branch.
    func_space = V
    count = 0
    while func_space.num_sub_spaces() > 0:
        if count > 10:
            raise ValueError("Avoiding an infinite loop.")
        func_space = func_space.sub(0).collapse()
        count += 1

    n_cells = func_space.mesh().num_cells()
    dofmap = func_space.dofmap()

    connectivity = list()
    for i in range(n_cells):
        cell_dofs_i = dofmap.cell_dofs(i)
        connectivity.append(cell_dofs_i)

    connectivity = np.array(connectivity, dtype=np.int)

    return connectivity


def get_vtk_cell_array(np_connectivity, permutation=slice(None)):
    """
    Create and return a vtkCellArray object given the 2D connectivity matrix.
    The user can optionally provide a permutation to map DOFs from the numbering
    convention used to write the connectivity matrix to the VTK convention.


    Parameters
    ----------

    np_connectivity : numpy.ndarray
        The mesh connectivity matrix given as a 2D NumPy array object.
    permutation : slice, list, tuple (default slice(None))
        The permutation to use in order to convert between dolfin's and
        a third-party (e.g. VTK) node numbering convention.


    Returns
    -------

    vtk_cells : vtk.vtkCellArray
        The cell array containing the node connectivity for each
        cell in the mesh.

    """
    import numpy as np
    import vtk
    from vtk.util.numpy_support import numpy_to_vtkIdTypeArray

    n_cells, n_nodes = np_connectivity.shape
    num_nodes = np.ones([n_cells, 1], dtype=np.int)*n_nodes
    arrs_to_concat = (num_nodes, np_connectivity[:, permutation])
    flat_connectivity = np.concatenate(arrs_to_concat, axis=1)
    flat_connectivity = flat_connectivity.flatten()
    vtk_connectivity = numpy_to_vtkIdTypeArray(flat_connectivity, deep=True)

    vtk_cells = vtk.vtkCellArray()
    vtk_cells.SetCells(n_cells, vtk_connectivity)

    return vtk_cells


def get_vtk_points(np_coordinates, geo_dim=3):
    """
    Create and return a vtkPoints object by giving the coordinate matrix as a
    1D or 2D array. The format for each, assuming 3D geometry, is the following:

    1D array: [x0, y0, z0, x1, y1, z1,..., xn, yn, zn]
    2D array: [[x0, y0, z0],[x1, y1, z1],...,[xn, yn, zn]]


    Parameters
    ----------

    np_coordinates : numpy.ndarray
        The mesh node coordinates given as a 2D NumPy array object.
    geo_dim : int (default 3)
        The geometric dimension. Used for padding the coordinate array
        if necessary.


    Returns
    -------

    vtk_points : vtk.vtkPoints
        The vtkPoints object storing 3D points information.

    """
    import numpy as np
    import vtk
    from vtk.util.numpy_support import numpy_to_vtk

    np_coordinates = np_coordinates.reshape([-1, geo_dim])
    np_coordinates = _pad_array(np_coordinates, geo_dim)
    np_coordinates, indices = np.unique(np_coordinates, axis=0,
                                        return_index=True)
    np_coordinates = np_coordinates[np.argsort(indices), :]

    vtk_coordinates = numpy_to_vtk(np_coordinates, deep=True)
    vtk_points = vtk.vtkPoints()
    vtk_points.SetData(vtk_coordinates)

    return vtk_points


def get_vtk_vector_array(func):
    """
    Extract the coefficients used to represent a function within the FE
    function space.


    Parameters
    ----------

    func : dolfin.Function
        The FEM approximation to a function.


    Returns
    -------

    vtk_vec : vtk.vtkDoubleArray
        The VTK array object storing the coefficients used by the
        dolfin.Function object.

    """
    import numpy as np
    from vtk.util.numpy_support import numpy_to_vtk

    shape = func.ufl_shape
    len_shape = len(shape)
    if len_shape == 0:
        n_components = 1
        vtk_components = 1
    elif len_shape == 1:
        n_components = shape[0]
        vtk_components = 3
    else:
        raise NotImplementedError

    geo_dim = func.function_space().mesh().geometry().dim()
    if len_shape > 0:
        array = func.vector().get_local().reshape([-1, n_components])
        array = _pad_array(array, geo_dim)
    else:
        array = func.vector().get_local()

    vtk_vec = numpy_to_vtk(array, deep=True)
    vtk_vec.SetNumberOfComponents(vtk_components)
    vtk_vec.SetName(func.name())

    return vtk_vec


def create_vtk_unstructured_grid(vtk_points, vtk_cells, cell_type,
                                 vtk_vec_vals=None, data_type="points"):
    """
    Create a VTK unstructured grid object given the points, cells, cell
    type, field data, and the type of field data (cells vs. points).


    Parameters
    ----------

    vtk_points : vtk.vtkPoints
        The vtkPoints object storing 3D points information.
    vtk_cells : vtk.vtkCellArray
        The cell array containing the node connectivity for each
        cell in the mesh.
    cell_type : int
        The VTK integer ID for the cell type used in the mesh. All
        cells are assumed to be of the same type.
    vtk_vec_vals : vtk.vtkDoubleArray (default None)
        The VTK array object storing field data.
    data_type : str (default "points")
        Specify whether the field data is 'points' data or 'cells' data.

    Returns
    -------

    ugrid : vtk.vtkUnstructuredGrid
        The unstructured grid object containing the mesh, and field data.

    """
    import vtk

    ugrid = vtk.vtkUnstructuredGrid()
    ugrid.SetPoints(vtk_points)
    ugrid.SetCells(cell_type, vtk_cells)
    if vtk_vec_vals is not None:
        if data_type == "points":
            ugrid.GetPointData().AddArray(vtk_vec_vals)
        elif data_type == "cells":
            ugrid.GetCellData().AddArray(vtk_vec_vals)

    return ugrid


def get_vtk_cell_type(V, degree_overwrite=None):
    """
    Get the integer ID for the matching VTK cell type. Both the name of the
    cell (triangle, hexahedron, etc.) and the polynomial degree are used to
    determine the cell type.

    Parameters
    ----------

    V : dolfin.FunctionSpace
        The function space to extract cell type from.

    Returns
    -------

    cell_type : int
        The VTK integer ID for the cell type used in the mesh. All
        cells are assumed to be of the same type.
    permutation : tuple
        The permutation to use in order to convert between dolfin's and
        VTK's node numbering convention.

    """
    import vtk
    import dolfin as dlf

    ufl_element = V.ufl_element()
    ufl_cell = ufl_element.cell()
    if degree_overwrite is None:
        degree = ufl_element.degree()
    else:
        degree = degree_overwrite

    if degree > 2:
        msg = "VTK does not support degree higher than 2."
        raise NotImplementedError(msg)

    if ufl_cell not in dlf_to_vtk_mappings:
        msg = "The dolfin cell type '{0}' is unrecognized. Please consider " \
            + "filing a bug report."
        raise NotImplementedError(msg.format(ufl_cell))

    vtk_cell_type = dlf_to_vtk_mappings[ufl_cell][degree]['vtk_cell_type']
    permutation = dlf_to_vtk_mappings[ufl_cell][degree]['permutation']

    return vtk_cell_type, permutation


def _get_num_nodes_per_cell(dofmap):
    """
    Get the number of nodes per cell for a given function space. A minor
    correction is necessary for non-scalar valued functions.


    Parameters
    ----------

    dofmap : dolfin.cpp.fem.DofMap
        The DOF map generated by dolfin that stores connectivity
        information between different mesh entities (vertices, edges,
        facets, and cells)


    Returns
    -------

    n_nodes_per_cell : int
        The number of nodes connected to a cell.

    """
    _cell_dofs = dofmap.cell_dofs(0) # Cell 0
    _n_dofs_per_node = dofmap.num_entity_dofs(0) # Entity dimension 0
    n_nodes_per_cell = _cell_dofs.size // _n_dofs_per_node
    return n_nodes_per_cell


def _pad_array(arr, geo_dim):
    """
    Pad given array with the number of zero columns and rows necessary
    to have a shape of (geo_dim,)*nrank at each data point (e.g. vertex
    or cell) where nrank = arr.ndim - 1.

    Parameters
    ----------

    arr : numpy.ndarray
    geo_dim : int
        The geometric dimension to use.

    """
    import numpy as np

    nrank = arr.ndim - 1
    shape_orig = arr.shape
    nentities = shape_orig[0]
    all_sl = tuple(slice(0, i) for i in shape_orig)

    # shape = (nentities,) + (geo_dim,)*nrank
    full_shape = (nentities,) + (3,)*nrank
    padded_arr = np.zeros(full_shape)
    padded_arr[all_sl] = arr

    return padded_arr


# The VTK cell type integer IDs, and the permutation (and its inverse)
# to convert between the FEniCS and VTK node numbering schemes.
from ufl import Cell as _Cell

_interval1d = _Cell("interval", 1)
_interval2d = _Cell("interval", 2)
_interval3d = _Cell("interval", 3)

_triangle2d = _Cell("triangle", 2)
_triangle3d = _Cell("triangle", 3)

_quadrilateral2d = _Cell("quadrilateral", 2)
_quadrilateral3d = _Cell("quadrilateral", 3)

_tetrahedron3d = _Cell("tetrahedron", 3)

_hexahedron3d = _Cell("hexahedron", 3)


import vtk as _vtk

dlf_to_vtk_mappings = {
    _interval1d: {
        1: {
            'vtk_cell_type': _vtk.VTK_LINE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        },
        2: {
            'vtk_cell_type': _vtk.VTK_QUADRATIC_EDGE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        }
    },
    _interval2d: {
        1: {
            'vtk_cell_type': _vtk.VTK_LINE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        },
        2: {
            'vtk_cell_type': _vtk.VTK_QUADRATIC_EDGE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        }
    },
    _interval3d: {
        1: {
            'vtk_cell_type': _vtk.VTK_LINE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        },
        2: {
            'vtk_cell_type': _vtk.VTK_QUADRATIC_EDGE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        }
    },
    _triangle2d: {
        1: {
            'vtk_cell_type':  _vtk.VTK_TRIANGLE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        },
        2: {
            'vtk_cell_type':  _vtk.VTK_QUADRATIC_TRIANGLE,
            'permutation': (0, 1, 2, 5, 3, 4),
            'permutation_inv': (0, 1, 2, 4, 5, 3)
        }
    },
    _triangle3d: {
        1: {
            'vtk_cell_type':  _vtk.VTK_TRIANGLE,
            'permutation': slice(None),
            'permutation_inv': slice(None)
        },
        2: {
            'vtk_cell_type':  _vtk.VTK_QUADRATIC_TRIANGLE,
            'permutation': (0, 1, 2, 5, 3, 4),
            'permutation_inv': (0, 1, 2, 4, 5, 3)
        }
    },
    _quadrilateral2d: {
        1: {
            'vtk_cell_type':  _vtk.VTK_QUAD,
            'permutation': (0, 1, 3, 2),
            'permutation_inv': (0, 1, 3, 2)
        },
        2: {
            'vtk_cell_type':  _vtk.VTK_BIQUADRATIC_QUAD,
            'permutation': (0, 1, 4, 3, 2, 7, 5, 6, 8),
            'permutation_inv': (0, 1, 4, 3, 2, 6, 7, 5, 8)
        }
    },
    _quadrilateral3d: {
        1: {
            'vtk_cell_type':  _vtk.VTK_QUAD,
            'permutation': (0, 1, 3, 2),
            'permutation_inv': (0, 1, 3, 2)
        },
        2: {
            'vtk_cell_type':  _vtk.VTK_BIQUADRATIC_QUAD,
            'permutation': (0, 1, 4, 3, 2, 7, 5, 6, 8),
            'permutation_inv': (0, 1, 4, 3, 2, 6, 7, 5, 8)
        }
    },
    _tetrahedron3d: {
        1: {
            'vtk_cell_type':  _vtk.VTK_TETRA,
            'permutation': slice(None),
            'permutation_inv': slice(None) # No permutation
        },
        2: {
            'vtk_cell_type':  _vtk.VTK_QUADRATIC_TETRA,
            'permutation': (0, 1, 2, 3, 9, 6, 8, 7, 5, 4),
            'permutation_inv': (0, 1, 2, 3, 9, 8, 5, 7, 6, 4)
        }
    },
    _hexahedron3d: {
        1: {
            'vtk_cell_type':  _vtk.VTK_HEXAHEDRON,
            'permutation': (0, 1, 3, 2, 4, 5, 7, 6),
            'permutation_inv': (0, 1, 3, 2, 4, 5, 7, 6)
        },
        2: {
            'vtk_cell_type':  _vtk.VTK_TRIQUADRATIC_HEXAHEDRON,
            'permutation': (0, 1, 4, 3, 9, 10, 13, 12, 2,
                            7, 5, 6, 11, 16, 14, 15, 18, 19,
                            22, 21, 24, 25, 20, 23, 8, 17, 26),
            'permutation_inv':  (0, 1, 8, 3, 2, 10, 11, 9, 24,
                                 4, 5, 12, 7, 6, 14, 15, 13, 25,
                                 16, 17, 22, 19, 18, 23, 20, 21, 26)
        }
    }
}


# (Dolfin cell name, polynomial degree)
vtk_to_dlf_mappings = {
    _vtk.VTK_LINE: ("interval", 1),
    _vtk.VTK_QUADRATIC_EDGE: ("interval", 2),
    _vtk.VTK_TRIANGLE: ("triangle", 1),
    _vtk.VTK_QUADRATIC_TRIANGLE: ("triangle", 2),
    _vtk.VTK_QUAD: ("quadrilateral", 1),
    _vtk.VTK_BIQUADRATIC_QUAD: ("quadrilateral", 2),
    _vtk.VTK_TETRA: ("tetrahedron", 1),
    _vtk.VTK_QUADRATIC_TETRA: ("tetrahedron", 2),
    _vtk.VTK_HEXAHEDRON: ("hexahedron", 1),
    _vtk.VTK_TRIQUADRATIC_HEXAHEDRON: ("hexahedron", 2)
}

vtk_to_dlf_cell_names = {
    _vtk.VTK_LINE: "interval",
    _vtk.VTK_QUADRATIC_EDGE: "interval",
    _vtk.VTK_TRIANGLE: "triangle",
    _vtk.VTK_QUADRATIC_TRIANGLE: "triangle",
    _vtk.VTK_QUAD: "quadrilateral",
    _vtk.VTK_BIQUADRATIC_QUAD: "quadrilateral",
    _vtk.VTK_TETRA: "tetrahedron",
    _vtk.VTK_QUADRATIC_TETRA: "tetrahedron",
    _vtk.VTK_HEXAHEDRON: "hexahedron",
    _vtk.VTK_TRIQUADRATIC_HEXAHEDRON: "hexahedron"
}

# Use the cell name instead of the ufl.Cell object as the key.
topological_dims = {
    'interval': 1,
    'triangle': 2,
    'quadrilateral': 2,
    'tetrahedron': 3,
    'hexahedron': 3
}

del _vtk, _get_logger, _Cell
