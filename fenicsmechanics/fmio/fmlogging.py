import logging as _logging
from ..dolfincompat import LogLevel as _LogLevel
from argparse import Namespace as _Namespace


__all__ = ["get_user_logger", "get_logger", "set_log_level", "init_logs"]

# Some default values for function parameters.
_default_logname = "fm_progress.out"
_default_stream_level = _LogLevel.INFO
_default_file_level = _LogLevel.DEBUG


# Add custom level and helper functions.
#     https://stackoverflow.com/a/13638084
_logging.addLevelName(_LogLevel.PROGRESS, "PROGRESS")
_logging.addLevelName(_LogLevel.TRACE, "TRACE")


# Set the level of the root logger to zero.
_root_logger = _logging.getLogger()
_root_logger.setLevel(_logging.NOTSET)
del _root_logger


def _progress(self, message, *args, **kwargs):
    if self.isEnabledFor(_LogLevel.PROGRESS):
        self._log(_LogLevel.PROGRESS, message, args, **kwargs)
    return None


def _trace(self, message, *args, **kwargs):
    if self.isEnabledFor(_LogLevel.TRACE):
        self._log(_LogLevel.TRACE, message, args, **kwargs)
    return None


# Add helper functions to Logger class for the additional
# logging levels.
_logging.Logger.progress = _progress
_logging.Logger.trace = _trace


# The idea of a custom formatter was obtained from:
#     https://stackoverflow.com/a/14859558
class LevelFormatter(_logging.Formatter):

    def __init__(self, **kwargs):
        # Pop this value because it's no longer needed.
        style = kwargs.pop('style', "%")

        # These values will be used by '_init_formats'
        fmt = kwargs.get('fmt')
        datefmt = kwargs.get('datefmt')

        super().__init__(fmt=fmt, datefmt=datefmt, style=style)

        # Replace 'fmt' with value set by logging.Formatter
        kwargs['fmt'] = self._style._fmt

        self._init_formats(**kwargs)

        return None


    def format(self, record):
        # Temporarily save the default format.
        fmt_orig = self._style._fmt

        # Determine which style to use and replace.
        self._style._fmt = self._select_format(record)

        # Ask the original formatter to do the work.
        result = _logging.Formatter.format(self, record)

        # Restore the original format.
        self._style._fmt = fmt_orig

        return result


    def _select_format(self, record):
        if record.levelno == _LogLevel.CRITICAL:
            fmt = self._formats.debug
        elif record.levelno == _LogLevel.ERROR:
            fmt = self._formats.trace
        elif record.levelno == _LogLevel.WARNING:
            fmt = self._formats.warning
        elif record.levelno == _LogLevel.INFO:
            fmt = self._formats.info
        elif record.levelno == _LogLevel.PROGRESS:
            fmt = self._formats.progress
        elif record.levelno == _LogLevel.TRACE:
            fmt = self._formats.trace
        elif record.levelno == _LogLevel.DEBUG:
            fmt = self._formats.debug
        else:
            msg = "The level '{0}' is not supported.".format(record.levelname)
            raise NotImplementedError(msg)
        return fmt


    def _init_formats(self, **kwargs):
        import re

        default = kwargs.pop('fmt', None)
        datefmt = kwargs.pop('datefmt', None)

        formats = self.default_formats()

        user_formats = kwargs.pop('formats', None)
        if user_formats is None:
            self._formats = formats
            return None

        # default_formats_dict = vars(default_formats)
        for k, v in vars(user_formats).items():
            if v is None:
                continue
            if hasattr(formats, k):
                setattr(formats, k, v)

        self._formats = formats

        return None


    @ staticmethod
    def default_formats():

        # List level name, module name, functon name, and line number as
        # header, and then write the message on the next, indented, line.
        verbose_fmt = "%(levelname)s - %(module)s.%(funcName)s " \
            + "- line: %(lineno)s\n\t%(message)s"

        # List the level name and function name.
        semiverbose_fmt = "%(levelname)s (%(funcName)s): %(message)s"

        # List the level name.
        basic_fmt = "%(levelname)s: %(message)s"

        # Only show the message.
        terse_fmt = "%(message)s"

        default_formats = _Namespace(
            default=terse_fmt,
            critical=verbose_fmt,
            error=verbose_fmt,
            warning=basic_fmt,
            info=terse_fmt,
            progress=basic_fmt,
            trace=semiverbose_fmt,
            debug=verbose_fmt
        )
        return default_formats


class LevelNoFilter(_logging.Filter):
    """
    Logging filter to messages based on the logging level in addition
    to the logger name.
    """
    def __init__(self, name="", level=_logging.NOTSET):
        super().__init__(name=name)
        if isinstance(level, str):
            levelno = getattr(_LogLevel, level)
        else:
            levelno = int(level)
        self.levelno = levelno
        return None


    def filter(self, record):
        # First, have the parent class check if this passes.
        base_filter_pass = super().filter(record)

        # Now do our own check based on the level.
        if record.levelno >= self.levelno:
            level_pass = True
        else:
            level_pass = False

        # Only pass if both filters approve.
        both_pass = (base_filter_pass and level_pass)

        return both_pass


def create_handlers(stream_level=_default_stream_level,
                    file_level=_default_file_level,
                    logname=_default_logname):

    # Initialize handler
    stream_handler = _logging.StreamHandler()

    # Create filters and add them to their respective handlers.
    stream_filter = LevelNoFilter(level=stream_level)
    stream_handler.addFilter(stream_filter)

    # Create an instance of the formatter and add to the two
    # handler objects.
    formatter = LevelFormatter()
    stream_handler.setFormatter(formatter)

    # Do the same with a file handler if the logname is not None.
    if logname is not None:
        file_handler = _logging.FileHandler(filename=logname)

        file_filter = LevelNoFilter(level=file_level)
        file_handler.addFilter(file_filter)

        file_handler.setFormatter(formatter)

    else:
        file_handler = None

    return stream_handler, file_handler


def create_logger(name, stream_level=_default_stream_level,
                  file_level=_default_file_level,
                  logname=_default_logname):
    logger = _logging.getLogger(name)

    # Add handlers only if the logger's parent is the root logger.
    root_logger = _logging.getLogger()
    if logger.parent is root_logger:

        stream_handler, file_handler = create_handlers(stream_level=stream_level,
                                                       file_level=file_level,
                                                       logname=logname)

        # Remove any previous handlers that might have been attached.
        _remove_all_handlers(logger)

        logger.addHandler(stream_handler)
        if file_handler is not None: # Check that this was created.
            logger.addHandler(file_handler)

    logger.setLevel(_logging.NOTSET)

    return logger


def create_fm_parent_logger(stream_level=_default_stream_level,
                            file_level=_default_file_level,
                            logname=_default_logname):
    logger = create_logger("fm", stream_level=stream_level,
                           file_level=file_level, logname=logname)
    return logger


def set_log_level(level="INFO", dlf_level=None):
    if isinstance(level, str):
        level = level.upper()
        if level not in ("INFO", "WARNING", "ERROR", "DEBUG"):
            msg = "The log level provided ({}) is not valid.".format(level)
            raise ValueError(msg)
        level = getattr(_logging, level)
    else:
        if not isinstance(level, int):
            msg = "The log level provided ({}) is not valid.".format(level)
            raise ValueError(msg)
    _logger.setLevel(level)

    if dlf_level is None:
        dlf_level = level
    _dlf.set_log_level(dlf_level)

    return None


def _remove_all_handlers(logger):
    for h in logger.handlers:
        logger.removeHandler(h)
    return None


def reconfigure_logger(logger, stream_level=_default_stream_level,
                       file_level=_default_file_level,
                       logname=_default_logname):

    for h in logger.handlers:
        logger.removeHandler(h)

    stream_handler, file_handler = create_handlers(stream_level=stream_level,
                                                   file_level=file_level,
                                                   logname=logname)

    logger.addHandler(stream_handler)
    if file_handler is not None:
        logger.addHandler(file_handler)
    logger.setLevel(_logging.NOTSET)

    return None


def reconfigure_list_of_loggers(all_loggers,
                                stream_level=_default_stream_level,
                                file_level=_default_file_level,
                                logname=_default_logname):
    for logger in all_loggers:
        if logger is None:
            continue
        else:
            reconfigure_logger(logger, stream_level=stream_level,
                               file_level=file_level, logname=logname)
    return None


def get_existing_loggers(loggernames):

    if isinstance(loggernames, str):
        loggernames = [loggernames]

    for name in loggernames:
        if name in _logging.Logger.manager.loggerDict:
            ret = _logging.getLogger(name)
        else:
            ret = None
        yield ret
    return None


def get_logger(name="fm", stream_level=_default_stream_level,
               file_level=_default_file_level, logname=_default_logname):
    # First create the logger with our handlers and filters if it does not
    # exist already.
    if name not in _logging.Logger.manager.loggerDict:
        create_logger(name, stream_level=stream_level,
                      file_level=file_level, logname=logname)


    return _logging.getLogger(name)


def get_user_logger(stream_level=_default_stream_level,
                    file_level=_default_file_level,
                    logname=_default_logname):
    user_logger = get_logger("fm.usermessages", stream_level=stream_level,
                             file_level=file_level, logname=logname)
    return user_logger


def init_logs(stream_level=_default_stream_level,
              file_level=_default_file_level,
              logname=_default_logname,
              reconfigure_others=False):

    # Create our own parent logger.
    create_fm_parent_logger(stream_level=stream_level,
                            file_level=file_level,
                            logname=logname)

    # Reconfigure the FEniCS loggers to output to the same file
    # and stream as our own loggers.
    if reconfigure_others:
        dolfin_loggers = get_existing_loggers(["UFL", "FFC", "dijitso"])
        reconfigure_list_of_loggers(dolfin_loggers, stream_level=stream_level,
                                    file_level=file_level, logname=logname)

    return None
