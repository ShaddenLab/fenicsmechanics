import numpy as _np
import dolfin as _dlf

__all__ = ["compute_outer_normals", "get_averaged_vertex_normals",
           "remove_vector_component", "remove_vector_function_component"]


def compute_outer_normals(mesh):

    geo_dim = mesh.geometry().dim()
    fct_normals = dict()
    vtx_normals = dict()
    for i in range(mesh.num_vertices()):
        vtx_normals[i] = _np.zeros(geo_dim)

    # if isinstance(mesh, _dlf.BoundaryMesh):
    #     iterator_func = _dlf.cells
    # else:
    #     iterator_func = _dlf.facets

    cellname = mesh.ufl_cell().cellname()
    if cellname in ["triangle", "quadrilateral"]:
        iterator_func = _dlf.cells

        # Initialize cell orientations and use this to make sure
        # all normals are pointing in the same direction.
        n_ref = _dlf.Constant([0., 0., 1.])
        mesh.init_cell_orientations(n_ref)

    elif cellname in ["tetrahedron", "hexahedron"]:
        iterator_func = _dlf.facets
    else:
        msg = "Don't know how to compute the normal for cells of " \
            + "type {0}."
        raise TypeError(msg.format(cellname))

    for fct in iterator_func(mesh):

        if isinstance(fct, _dlf.Facet):
            if not fct.exterior():
                continue
            n = fct.normal().array()
        else:
            # Make sure all cell normals point in the same
            # general direction.
            exponent = fct.orientation()
            sgn = (-1)**exponent
            n = sgn*fct.cell_normal().array()

        idx = fct.index()
        fct_normals[idx] = n[:geo_dim]
        for v_idx in fct.entities(0):
            vtx_normals[v_idx] += n[:geo_dim]

    for i, n_vec in vtx_normals.items():
        mag = _np.linalg.norm(n_vec)
        if _np.abs(mag) <= 1e-12:
            continue
        n_vec /= mag

    for i in range(mesh.num_vertices()):
        if i not in vtx_normals:
            vtx_normals[i] = _np.zeros(3)

    return fct_normals, vtx_normals


def get_averaged_vertex_normals(mesh):

    mesh.init()

    fct_normals, vtx_normals = compute_outer_normals(mesh)

    V = _dlf.VectorFunctionSpace(mesh, "CG", 1)
    dof2vx = _dlf.dof_to_vertex_map(V)

    n_func = _dlf.Function(V)
    n_vec = n_func.vector()

    vtx_normal_components = _np.stack(tuple(vtx_normals.values()), axis=0)
    vtx_normal_components = vtx_normal_components.flatten()[dof2vx]
    n_vec.set_local(vtx_normal_components)

    return n_func


def remove_vector_component(u, vtx_idx, component, val=0.0, normalize=False):

    u_vec = u.vector()
    Vu = u.function_space()
    mesh = Vu.mesh()

    V = _dlf.VectorFunctionSpace(mesh, "CG", 1)
    geo_dim = mesh.geometry().dim()

    # Get Vertex to DOF index map
    v2d_flat = _dlf.vertex_to_dof_map(V)
    v2d = v2d_flat.reshape([-1, geo_dim])

    ## Set the given component to provided value.
    vtx_dof_ids = v2d[vtx_idx]
    if component == "all":
        u_vec[vtx_dof_ids] = val
    elif component.lower() == "x":
        u_vec[vtx_dof_ids[0]] = val
    elif component.lower() == "y":
        u_vec[vtx_dof_ids[1]] = val
    elif component.lower() == "z":
        u_vec[vtx_dof_ids[2]] = val
    else:
        raise ValueError

    if normalize:
        mag = _np.linalg.norm(u_vec[vtx_dof_ids])
        u_vec[vtx_dof_ids] /= mag

    return None


def remove_vector_function_component(mesh, fct_function, u, face_ids,
                                     component="all", val=0.0, normalize=False):

    mesh.init() # Make sure entity maps are initialized.
    top_dim = mesh.topology().dim()

    for vtx in _dlf.vertices(mesh):
        fcts = vtx.entities(top_dim - 1)
        all_vtx_face_ids = list()
        for fct_id in fcts:
            fct = _dlf.Facet(mesh, fct_id)
            fct_face_id = fct_function[fct]
            all_vtx_face_ids.append(fct_face_id)

        set_vtx_face_ids = set(all_vtx_face_ids)
        intersection = set_vtx_face_ids.intersection(face_ids)
        if len(intersection) == len(face_ids):
            remove_vector_component(u, vtx.index(), component,
                                    val=val, normalize=normalize)

    return None


def find_mesh_inner_outer_boundaries(mesh, inner_id=20, outer_id=30):
    """
    Create a facet function identifying the inner and outer boundaries
    of a hollow mesh.

    Parameters
    ----------

    mesh : dolfin.Mesh

    Returns
    -------

    facet_function : dolfin.cpp.mesh.MeshFunctionSizet

    """

    # Make sure all connectivity tables necessary are created.
    mesh.init()

    top_dim = mesh.topology().dim()
    geo_dim = mesh.geometry().dim()

    facet_function = _dlf.MeshFunction("size_t", mesh, top_dim - 1)
    facet_function.set_all(0)

    if geo_dim == top_dim:
        # Use the normal vector of the facet.
        tag_mesh_boundaries_using_normals(mesh, facet_function, geo_dim,
                                          inner_id=inner_id, outer_id=outer_id)
    else:
        # Compare midpoints of the facet and the cell to determine which
        # is further from centroid.
        tag_mesh_boundaries_using_midpoints(mesh, facet_function, geo_dim,
                                            inner_id=inner_id, outer_id=outer_id)

    return facet_function


def tag_mesh_boundaries_using_midpoints(mesh, facet_function, geo_dim,
                                        inner_id=20, outer_id=30):

    centroid = mesh.coordinates().mean(axis=0)

    for fct in _dlf.facets(mesh):
        if not fct.exterior():
            continue

        cell_id = fct.entities(fct.dim() + 1)[0]
        cell = _dlf.Cell(mesh, cell_id)

        cell_midpoint = cell.midpoint().array()[:geo_dim]
        fct_midpoint = fct.midpoint().array()[:geo_dim]

        cell_distance = _np.linalg.norm(cell_midpoint - centroid)
        fct_distance = _np.linalg.norm(fct_midpoint - centroid)

        if fct_distance < cell_distance:
            facet_function.set_value(fct.index(), inner_id)
        else:
            facet_function.set_value(fct.index(), outer_id)

    return None


def tag_mesh_boundaries_using_normals(mesh, facet_function, geo_dim,
                                      inner_id=20, outer_id=30):

    centroid = mesh.coordinates().mean(axis=0)

    for fct in _dlf.facets(mesh):
        if not fct.exterior():
            continue
        normal = fct.normal().array()[:geo_dim]
        midpoint = fct.midpoint().array()[:geo_dim]
        x = midpoint - centroid
        dotprod = x.dot(normal)

        if dotprod < 0.0:
            facet_function.set_value(fct.index(), inner_id)
        else:
            facet_function.set_value(fct.index(), outer_id)

    return None
