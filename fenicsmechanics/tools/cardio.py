from ..exceptions import InconsistentCombination
from ..fmio import fmconversions as _fmc

import dolfin as _dlf

_meshfunction_types = (_dlf.cpp.mesh.MeshFunctionBool,
                       _dlf.cpp.mesh.MeshFunctionDouble,
                       _dlf.cpp.mesh.MeshFunctionInt,
                       _dlf.cpp.mesh.MeshFunctionSizet)

def create_dirichlet_bcs(V, region_identifiers, region_ids,
                         values, method="topological"):
    """
    Create a set of dolfin.DirichletBC objects.


    Parameters
    ----------

    V : dolfin.FunctionSpace
        The function space for which boundary conditions are to be specified.
    region_identifiers : list, dolfin.SubDomain
                         dolfin.MeshFunction[Bool|Double|Int|Size]
        Object(s) used to identify regions on which boundary conditions are
        to be applied. Note that the region IDs are not used if a SubDomain
        object is provided, and thus 'None' can be provided.
    region_ids : list, tuple
        Integer IDs assigned to boundary regions of interest.
    values : list, tuple (default [0.0, 1.0])
        Values to assign at the boundary regions of interest.
    method : str, list, tuple (default "topological")
        Method used to identify the DOFs on the boundary regions of interest.
        See the documentation for dolfin.DirichletBC for more information.


    Returns
    -------

    dirichlet_bcs : list
        List of dolfin.DirichletBC objects.

    """
    from numbers import Number
    from collections.abc import Sequence

    n_ids = len(region_ids)
    if isinstance(region_identifiers, (list, tuple)):
        n_identifiers = len(region_identifiers)

        if n_identifiers != n_ids:
            msg = "The number of region identifiers ({0}) and the number of " \
                + "region IDs ({1}) must match when multiple region " \
                + "are provided."
            raise InconsistentCombination(msg.format(n_identifiers, n_ids))
    else:
        region_identifiers = [region_identifiers]*n_ids

    # Check number of methods provided
    if isinstance(method, str):
        method = [method]*n_ids
    elif isinstance(method, (list, tuple)):
        n_methods = len(method)
        if n_ids != n_methods:
            msg = "The number of methods ({0}) and the number of " \
                + "region IDs ({1}) must match when multiple methods " \
                + "are provided."
            raise InconsistentCombination(msg.format(n_methods, n_ids))
    else:
        msg = "Method '{0}' is not understood in this context."
        raise ValueError(msg.format(method))

    # Check number of values provided.
    if isinstance(values, (list, tuple)):
        n_vals = len(values)
        if n_ids != n_vals:
            msg = "The number of values assigned ({0}) and the number of " \
                + "region IDs ({1}) must match when multiple values " \
                + "are provided."
            raise InconsistentCombination(msg.format(n_vals, n_ids))
    elif isinstance(values, Number):
        values = [values]*n_ids
    else:
        msg = "Don't understand how to assign the value '{0}'."
        raise ValueError(msg.format(values))

    dirichlet_bcs = list()
    region_info = zip(region_identifiers, values, region_ids, method)
    for region, val, r_id, mthd in region_info:
        if isinstance(region, _dlf.SubDomain):
            bc = _dlf.DirichletBC(V, val, region, method=mthd)
        else:
            bc = _dlf.DirichletBC(V, val, region, r_id, method=mthd)
        dirichlet_bcs.append(bc)

    return dirichlet_bcs


def solve_laplacian(mesh, region_identifier, region_ids, degree=1,
                    bc_values=[0.0, 1.0], method="topological"):
    """
    Solve Laplace's equation on the given mesh, the given region identifier(s)
    and IDs, and boundary values.


    Parameters
    ----------

    mesh : dolfin.Mesh
        Mesh object used to discretize the domain of interest.
    region_identifier : dolfin.MeshFunction[Bool|Double|Int|Size], dolfin.SubDomain
        Object used to identify regions on which boundary conditions are
        to be applied.
    region_ids : list, tuple
        Integer IDs assigned to boundary regions of interest.
    degree : int (default 1)
        Polynomial degree of the FE function space.
    bcs_values : list, tuple (default [0.0, 1.0])
        Values to assign at the boundary regions of interest.
    method : str, list, tuple (default "topological")
        Method used to identify the DOFs on the boundary regions of interest.
        See the documentation for dolfin.DirichletBC for more information.

    Returns
    -------

    phi : dolfin.Function
        The solution to Laplace's equation with the given boundary conditions.

    """

    if degree == 0:
        family = "DG"
    else:
        family = "CG"
    V = _dlf.FunctionSpace(mesh, family, degree)

    bcs = create_dirichlet_bcs(V, region_identifier, region_ids,
                               bc_values, method=method)

    xi = _dlf.TestFunction(V)
    dphi = _dlf.TrialFunction(V)
    phi = _dlf.Function(V)

    f = _dlf.Constant(0.0)
    a = _dlf.inner(_dlf.grad(xi), _dlf.grad(dphi))*_dlf.dx
    L = xi*f*_dlf.dx
    _dlf.solve(a == L, phi, bcs)

    return phi


def compute_discontinuous_gradient(phi, normalize=True, Vvec=None):
    """
    Compute the gradient of the given scalar function. The gradient is computed
    on a Discontinuous Galerkin finite element, where the polynomial degree is
    one less than that used to represent the given scalar function, unless a
    function space object is provided.


    Parameters
    ----------

    phi : dolfin.Coefficient
        The function whose gradient is to be computed.
    normalize : bool (default True)
        Specify whether to normalize the resulting vector field.
    Vvec : dolfin.FunctionSpace (default None)
        Function space used to represent vector the gradient. If no function
        space is provided, one will be created.


    Returns
    -------

    vecfield : dolfin.Function
        The resulting gradient.


    """
    Vphi = phi.function_space()

    grad_phi = _dlf.grad(phi)
    if normalize:
        grad_phi_norm = _dlf.sqrt(_dlf.dot(grad_phi, grad_phi))
        f = grad_phi/grad_phi_norm
    else:
        f = grad_phi

    if Vvec is None:
        mesh = Vphi.mesh()
        degree = Vphi.ufl_element().degree()
        Vvec = _dlf.VectorFunctionSpace(mesh, "DG", degree-1)

    xi = _dlf.TestFunction(Vvec)
    du = _dlf.TrialFunction(Vvec)
    vecfield = _dlf.Function(Vvec)

    a = _dlf.dot(xi, du)*_dlf.dx
    L = _dlf.dot(xi, f)*_dlf.dx

    _dlf.solve(a == L, vecfield)

    return vecfield


def project_vector_field(vecfield, degree=0, family="DG"):
    """
    Project the vector field represented in one function space to a new
    function space. Note that the same object is returned if the new
    function space is the same as the original.


    Parameters
    ----------

    vecfield : dolfin.Coefficient
        The vector field to be projected.
    degree : int (default 0)
        The polynomial degree of the new function space.
    family : str (default "DG")
        The element family of the new function space.


    Returns
    -------

    projected_vecfield : dolfin.Coefficient
        The projected function.

    """
    V_orig = vecfield.function_space()
    orig_el = V_orig.ufl_element()

    mesh = V_orig.mesh()
    vec_el = _dlf.VectorElement(family, mesh.ufl_cell(), degree)

    if orig_el == vec_el:
        # The finite elements are the same, thus there is no need to
        # solve anything.
        project_vector_field = vecfield
    else:
        V = _dlf.FunctionSpace(mesh, vec_el)
        projected_vecfield = _dlf.project(vecfield, V)

    return projected_vecfield


def compute_fiber_basis_frame(mesh, fct_function, region_id_pairs, degree=1,
                              bc_values=[0.0, 1.0], method="topological",
                              vec_degree=0, vec_family="DG", normalize=True,
                              ret_scalarfields=False):
    """
    Compute vector fields based on the region ID pairs given. The vector fields
    are the gradient of the solution to Laplace's equation with the constant
    BC values on the region pairs specified.


    Parameters
    ----------

    mesh : dolfin.Mesh
        Mesh object used to discretize the domain of interest.
    fct_function : dolfin.MeshFunction[Bool|Double|Int|Sizet]
        Object(s) used to identify regions on which boundary conditions
        are to be applied.
    region_id_pairs : list
        Pairs of integer IDs assigned to boundary regions of interest. If
        3 pairs are given, Laplace's equation will be solved three times.
    degree : int (default 1)
        The polynomial degree of the function space used to solve
        Laplace's equation. The "DG" family is used when degree is 0,
        and "CG" is used in all of the other cases.
    bc_values : list (default [0.0, 1.0])
        The values assigned to the pair of regions provided. Different pairs
        can be provided for each pair of region IDs. However, the same values
        will be used if only one pair is given.
    method : str (default "topological")
        The method used to identify degrees of freedom on the Dirichlet
        region. Look at the documentation for dolfin.DirichletBC for more
        information.
    vec_degree : int (default 0)
        The polynomial degree of the function space used to project the
        gradient of the computed scalar field.
    vec_family : str (default "DG")
        The element family of the function space used to project the gradient
        of the computed scalar field.
    normalize : bool (default True)
        Specify whether the resulting gradient of the vector field should be
        normalized.
    ret_scalarfields : bool (default False)
        Specify whether to return the solutions to Laplace's equation as well.


    Returns
    -------

    all_vecfields : list of dolfin.Function objects
        The projected (normalized) vector fields.
    all_phis : list of dolfin.Function objects (optional)
        The solutions to Laplace's equation.

    """
    n_id_pairs = len(region_id_pairs)

    if not isinstance(fct_function, (list, tuple)):
        fct_function = [fct_function]*n_id_pairs

    bc_vals_are_lists = all(isinstance(bc_val, (list, tuple))
                            for bc_val in bc_values)
    if not bc_vals_are_lists:
        bc_values = [bc_values]*n_id_pairs

    if isinstance(method, (list, tuple)):
        method = [method]*n_id_pairs

    all_vecfields = list()
    if ret_scalarfields:
        all_phis = list()
    Vvec = _dlf.VectorFunctionSpace(mesh, vec_family, vec_degree)
    for region_ids in region_id_pairs:
        phi = solve_laplacian(mesh, fct_function, region_ids, degree=degree)
        if ret_scalarfields:
            all_phis.append(phi)
        vecfield = compute_discontinuous_gradient(phi, Vvec=Vvec,
                                                  normalize=normalize)
        all_vecfields.append(vecfield)

    if ret_scalarfields:
        ret = all_vecfields, all_phis
    else:
        ret = all_vecfields
    return ret


def compute_binormal(e0, e1, normalize=True, Vvec=None):
    """
    Compute the vector field normal to the two vector fields
    provided, (e0 x e1).


    Parameters
    ----------

    e0 : dolfin.Function
        First vector field.
    e1 : dolfin.Function
        Second vector field.
    normalize : bool (default True)
        Specify whether the resulting vector field should be normalized.
    Vvec : dolfin.FunctionSpace (default None)
        The function space used to represent the binormal. If none is
        provided, the function space for 'e0' is used.


    Returns
    -------

    binormal : dolfin.Function
        The computed vector field normal to e0 and e1.

    """
    if Vvec is None:
        Vvec = e0.function_space()
    binormal = _dlf.Function(Vvec)
    xi = _dlf.TestFunction(Vvec)
    du = _dlf.TrialFunction(Vvec)

    binormal_val = _dlf.cross(e0, e1)
    if normalize:
        binormal_mag = _dlf.sqrt(_dlf.dot(binormal_val, binormal_val))
    else:
        binormal_mag = _dlf.Constant(1.0)
    val = binormal_val/binormal_mag

    a = _dlf.dot(xi, du)*_dlf.dx
    L = _dlf.dot(xi, val)*_dlf.dx

    _dlf.solve(a == L, binormal)

    return binormal


def rotate_vecfield(theta0, theta1, phi, axis, flat, Vvec=None):
    """
    Rotate a 'flat' vector field between angles 'theta0' and 'theta1'
    at the rate of 'phi' along an 'axis'.


    Parameters
    ----------

    theta0 : scalar constant
        The starting angle.
    theta1 : scalar constant
        The ending angle.
    phi : dolfin.Coefficient
        The rate of change in angle.
    axis : dolfin.Coefficient
        Vector field representing the axis to rotate with respect to.
    flat : dolfin.Coefficient
        The vector field to be rotated.
    Vvec : dolfin.FunctionSpace (default None)
        The function space used to represent the rotated vector field.
        If none is provided, the function space for 'flat' is used.


    Returns
    -------

    rotated : dolfin.Function
        The rotated vector field.

    """
    theta = (theta1 - theta0)*phi + theta0
    rotated_val = flat + _dlf.sin(theta)*_dlf.cross(axis, flat)
    rotated_val += _dlf.Constant(2.0)*_dlf.sin(theta/2.0)**2 \
                   *(_dlf.dot(axis, flat)*axis - flat)

    if Vvec is None:
        Vvec = flat.function_space()
    rotated = _dlf.Function(Vvec)
    xi = _dlf.TestFunction(Vvec)
    du = _dlf.TrialFunction(Vvec)
    a = _dlf.dot(xi, du)*_dlf.dx
    L = _dlf.dot(xi, rotated_val)*_dlf.dx
    _dlf.solve(a == L, rotated)

    return rotated


def compute_interior_curves_mesh(phi, val=[0.5]):
    import vtk

    if isinstance(val, (float, int)):
        val = [val]

    vtk_phi = _fmc.dolfin_to_vtk(phi)
    vtk_phi.GetPointData().SetActiveScalars(phi.name())

    contour_filter = vtk.vtkContourFilter()
    for i, v in enumerate(val):
        contour_filter.SetValue(i, v)
    contour_filter.SetInputData(vtk_phi)
    contour_filter.Update()

    interior_curves_mesh = _separate_curve_meshes(contour_filter)
    if len(interior_curves_mesh) == 1:
        interior_curves_mesh = interior_curves_mesh[0]

    return interior_curves_mesh


def compute_interior_curves(phi, val=[0.5], tol=1e-4,
                            reorder=False, geo_dim=2):
    import vtk
    from vtk.util import numpy_support as ns

    if isinstance(val, (float, int)):
        val = [val]

    vtk_phi = _fmc.dolfin_to_vtk(phi)
    vtk_phi.GetPointData().SetActiveScalars(phi.name())

    contour_filter = vtk.vtkContourFilter()
    for i, v in enumerate(val):
        contour_filter.SetValue(i, v)
    contour_filter.SetInputData(vtk_phi)
    contour_filter.Update()

    cf_output = contour_filter.GetOutput()

    all_pts = list()

    for i in range(contour_filter.GetNumberOfContours()):

        val = contour_filter.GetValue(i)
        lower = val - tol
        upper = val + tol

        threshold = vtk.vtkThreshold()
        threshold.SetInputData(cf_output)
        threshold.ThresholdBetween(lower, upper)
        threshold.Update()

        tmp_output = threshold.GetOutput()

        vtk_pts = tmp_output.GetPoints().GetData()
        _pts = ns.vtk_to_numpy(vtk_pts)[:, :geo_dim]

        if reorder:
            from meshgeneration.curveops import reorder_path
            pts = reorder_path(_pts)
        else:
            pts = _pts

        all_pts.append(pts)

    return all_pts


def _separate_curve_meshes(contour_filter, tol=1e-4):
    import vtk

    # Make sure it is updated before we do anything else.
    contour_filter.Update()
    cf_output = contour_filter.GetOutput()

    interior_curves_mesh = list()

    for i in range(contour_filter.GetNumberOfContours()):

        val = contour_filter.GetValue(i)
        lower = val - tol
        upper = val + tol

        threshold = vtk.vtkThreshold()
        threshold.SetInputData(cf_output)
        threshold.ThresholdBetween(lower, upper)
        threshold.Update()

        tmp_output = threshold.GetOutput()

        tmp_phi = _fmc.vtk_to_dolfin(tmp_output)
        tmp_mesh = tmp_phi.function_space().mesh()

        interior_curves_mesh.append(tmp_mesh)

    return interior_curves_mesh
