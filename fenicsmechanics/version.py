"""

"""
from collections import namedtuple as _namedtuple

_version_field_names = ("major", "minor", "micro", "patch")
_version_info_namedtuple = _namedtuple("version_info", _version_field_names)

def convert_version_string(version_string):
    split_string = version_string.split(".")
    int_vals = map(int, split_string[:3])
    remaining_components = ".".join(split_string[3:])
    if remaining_components == "":
        remaining_components = None
    version_tuple = _version_info_namedtuple(*int_vals, remaining_components)
    return version_tuple

def convert_version_tuple(version_tuple):
    version_string_tuple = map(str, version_tuple)
    version_string = ".".join(version_string_tuple)
    return version_string

_version_info = _version_info_namedtuple(2, 0, 0, "dev0")
_version_string = convert_version_tuple(_version_info)
