"""
This module checks the FEniCS version and provides handles to
variables that have changed in between different FEniCS versions.

Variables defined:
* MPI_COMM_WORLD
* LOG_LEVEL_ERROR

"""
# Use name space object to hold Log Level and MPI handles.
from argparse import Namespace as _Namespace

import dolfin as _dlf

from .version import convert_version_string as _convert_version

__all__ = ["MPI", "LogLevel", "DOLFIN_VERSION_INFO"]


def _inject_attributes(new_namespace, orig_namespace):
    """
    Add attributes from 'orig_namespace' to 'new_namespace', so long as they
    do not contain two consecutive underscores ('__') in the name.
    """
    # # This implementation causes methods to become static...why?
    # for k, v in vars(orig_namespace).items():
    #     if "__" not in k:
    #         print("k = ", v)
    #         setattr(new_namespace, k, v)

    for k in vars(orig_namespace).keys():
        if "__" not in k:
            # This prevents it from becoming a static method.
            obj = getattr(orig_namespace, k)
            try:
                obj = int(obj)
            except (TypeError, ValueError):
                pass
            setattr(new_namespace, k, obj)
    return None


# MPI and LogLevel name spaces following the newer FEniCS names.
LogLevel = _Namespace()
MPI = _Namespace()

# Will need to add others for older versions.
_inject_attributes(MPI, _dlf.MPI)

DOLFIN_VERSION_INFO = _convert_version(_dlf.__version__)

if DOLFIN_VERSION_INFO.major >= 2018:
    # Note: all MPI related objects seem to have been moved within 'dolfin.MPI'
    # for these versions, hence no updates necessary, with the exception of the
    # function 'compute_local_range', which was renamed to 'local_range'.

    _inject_attributes(LogLevel, _dlf.LogLevel)
    try:
        delattr(LogLevel, "name")
    except AttributeError:
        pass
else:
    MPI.comm_world = _dlf.mpi_comm_world()
    MPI.comm_self = _dlf.mpi_comm_self()

    LogLevel.CRITICAL = int(_dlf.CRITICAL)
    LogLevel.ERROR = int(_dlf.ERROR)
    LogLevel.WARNING = int(_dlf.WARNING)
    LogLevel.INFO = int(_dlf.INFO)
    LogLevel.PROGRESS = int(_dlf.PROGRESS)
    LogLevel.TRACE = int(_dlf.TRACE)
    LogLevel.DEBUG = int(_dlf.DEBUG)

# Clean up variables
del _Namespace, _dlf, _convert_version
